FROM python:alpine

RUN apk add -U curl wget bash gcc libc-dev linux-headers

RUN pip install requests

RUN curl https://raw.githubusercontent.com/immunIT/octowire-framework/master/install.py | python

ENTRYPOINT /bin/bash